import numpy as np

if __name__ == "__main__":

    print(" ----------------------- ZAD 1 -----------------------")
    print("v1 = (2i + 3j - 4k) + (-i + 4j - k)")
    v1 = np.add(np.array([2, 3, -4]), np.array([-1, 4, -1]))
    vectorSum = "{}i + {}j + {}k".format(v1[0], v1[1], v1[2])
    print("v1 = ", vectorSum)
    print("----------")

    print("s = v1 * (-i + 4j - k)")
    s = np.dot(v1, np.array([-1, 4, -1]))
    vectorScalarProduct = s
    print("s = ", vectorScalarProduct)
    print("----------")

    print("v2 = v1 x (2i + 2j + 4k)")
    v2 = np.cross(v1, np.array([2, 2, 4]))
    vectorVectorProduct = "{}i + {}j + {}k".format(v2[0], v2[1], v2[2])
    print("v2 = ", vectorVectorProduct)
    print("----------")

    v2Norm = np.linalg.norm(v2)
    print("|v2| = {:.4f}".format(v2Norm))

    print("v3 = v2 / |v2|")
    v3 = v2 / v2Norm
    v2UnitVector = "{:.4f}i + {:.4f}j + {:.4f}k".format(v3[0], v3[1], v3[2])
    print("v3 = ", v2UnitVector)
    print("----------")

    print("v4 = -v2")
    v4 = -v2
    oppositeDirectionVector = "{}i + {}j + {}k".format(v4[0], v4[1], v4[2])
    print("v4 = ", oppositeDirectionVector)
    print("----------")

    A = np.array([[1, 2, 3], [2, 1, 3], [4, 5, 1]])
    B = np.array([[-1, 2, -3], [5, -2, 7], [-4, -1, 3]])
    M1 = A + B
    print("M1 = ")
    for row in M1:
        print("{:10d}{:10d}{:10d}".format(row[0], row[1], row[2]))
    print("----------")

    M2 = np.matmul(A, np.transpose(B))
    print("M2 = ")
    for row in M2:
        print("{:10d}{:10d}{:10d}".format(row[0], row[1], row[2]))
    print("----------")

    try:
        B = np.array([[-1, 2, -3], [5, -2, 7], [-4, -1, 3]])
        Bdet = np.linalg.det(B)
        print("det(B) = {:14.4f}".format(Bdet))
        Binv = np.linalg.inv(B)
        print("Binv = ")
        for row in Binv:
            print("{:14.4f}{:14.4f}{:14.4f}".format(row[0], row[1], row[2]))

        M3 = np.matmul(A, Binv)
        print("M3 = ")
        for row in M3:
            print("{:14.4f}{:14.4f}{:14.4f}".format(row[0], row[1], row[2]))

        # BBinvProduct = np.matmul(B, Binv)
        # for row in BBinvProduct:
        #    print("{:14.4f}{:14.4f}{:14.4f}".format(row[0], row[1], row[2]))
    except np.linalg.LinAlgError as nonInvError:
        print("Cannot invert matrix B: " + nonInvError)

    print("----------")

    print()
    print(" ----------------------- ZAD 2 -----------------------")
    #  example enter: 1 1 1 6 -1 -2 1 -2 2 1 3 13
    a1, b1, c1, d1, a2, b2, c2, d2, a3, b3, c3, d3 = list(
        map(int, input("enter coefficients (separate by one space): ").split(" ")))
    abcCoefs = np.array([[a1, b1, c1], [a2, b2, c2], [a3, b3, c3]])
    dCoefs = np.array([[d1], [d2], [d3]])
    try:
        x, y, z = np.matmul(np.linalg.inv(abcCoefs), dCoefs)
        print("[x, y, z] = [{:14.4f}, {:14.4f}, {:14.4f}]".format(x[0], y[0], z[0]))
    except np.linalg.LinAlgError as nonInvError:
        print("Cannot invert matrix with coefficients for x, y, z: ")
    print("----------")

    print()
    print(" ----------------------- ZAD 3 -----------------------")
    Ax, Ay, Az = list(map(int, input("enter coordinates for triangle point A (separate by one space): ").split(" ")))  # 1, 0, 0
    Bx, By, Bz = list(map(int, input("enter coordinates for triangle point B (separate by one space): ").split(" ")))  # 5, 0, 0
    Cx, Cy, Cz = list(map(int, input("enter coordinates for triangle point C (separate by one space): ").split(" ")))  # 3, 8, 0
    Tx, Ty, Tz = list(map(int, input("enter coordinates for point T (separate by one space): ").split(" ")))  # 3, 4, 0
    abcCoefs = np.array([[Ax, Bx, Cx], [Ay, By, Cy], [Az, Bz, Cz]])
    tCoefs = np.array([[Tx], [Ty], [Tz]])
    try:
        t1, t2, t3 = np.matmul(np.linalg.inv(abcCoefs), tCoefs)
        print("[t1, t2, t3] = [{:14.4f}, {:14.4f}, {:14.4f}]".format(t1[0], t2[0], t3[0]))
    except np.linalg.LinAlgError as nonInvError:
        for i in range(len(abcCoefs)):
            if np.count_nonzero(abcCoefs[i]) == 0:
                abcCoefs[i] = np.ones(3)
                tCoefs[i] = 1
        print("abcCoefs = ")
        print(abcCoefs)
        print("tCoefs = ")
        print(tCoefs)
        t1, t2, t3 = np.matmul(np.linalg.inv(abcCoefs), tCoefs)
        print("[t1, t2, t3] = [{:14.4f}, {:14.4f}, {:14.4f}]".format(t1[0], t2[0], t3[0]))
    print("----------")


