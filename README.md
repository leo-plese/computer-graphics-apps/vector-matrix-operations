Vector Matrix Operations. Implemented in Python using NumPy library.

My lab assignment in Interactive Computer Graphics, FER, Zagreb.

Documentation in document "DokumentacijaIRGLabosi" under "1. VJEZBA."

Created: 2020
